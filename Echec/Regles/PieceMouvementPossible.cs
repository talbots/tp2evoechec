﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JeuSociete;

namespace Echec.Regles
{
    public class PieceMouvementPossible : IRegle
    {
        private Deplacement _deplacement;
        public PieceMouvementPossible(Deplacement deplacement)
        {
            _deplacement = deplacement;
        }

        public bool EstRespectee()
        {
            Piece pieceJouee = _deplacement.PieceCaseOrigine;
            bool piecePeutFaireDeplacement = pieceJouee.DeplacementPossible(_deplacement.Segment);
            return piecePeutFaireDeplacement;
        }

        public string ObtenirMessageErreur()
        {
            return "La pièce " + _deplacement.ObtenirTypePieceChoisie() + " ne peut pas se déplacer à cet endroit";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JeuSociete;

namespace Echec.Regles
{
    public class PionCapture : IRegle
    {
        private Deplacement _deplacementVoulu;
        private PlateauJeuQuadrille<Piece> _plateau;
        public PionCapture(Deplacement deplacement)
        {
            _deplacementVoulu = deplacement;
        }

        public bool EstRespectee()
        {
            if(_deplacementVoulu.ObtenirTypePieceChoisie() != TypesPiece.Pion || !_deplacementVoulu.Segment.EstSegmentDiagonal45Degres())
            {
                return true;//ne s'applique pas
            }
            if (CaptureEnnemi())
            {
                return true;
            }
            return false;
        }

        private bool CaptureEnnemi()
        {
            bool caseDestinationContientEnnemi = _deplacementVoulu.ContenuCaseDestination != null &&
                            _deplacementVoulu.ContenuCaseDestination.Couleur != _deplacementVoulu.PieceCaseOrigine.Couleur;
            return EstDeplacementUneCaseOblique() && caseDestinationContientEnnemi;
        }

        private bool EstDeplacementUneCaseOblique()
        {
            return _deplacementVoulu.Segment.EstSegmentDiagonal45Degres() && _deplacementVoulu.Segment.NombreCases() == 1;
        }

        public string ObtenirMessageErreur()
        {
            return "La pièce " + _deplacementVoulu.ObtenirTypePieceChoisie() + " faire un tel déplacement sans faire une capture";
        }
    }
}

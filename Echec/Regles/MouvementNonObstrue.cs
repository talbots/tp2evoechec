﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JeuSociete;

namespace Echec.Regles
{
    public class MouvementNonObstrue : IRegle
    {
        private readonly Deplacement _deplacementVoulu;
        private readonly PlateauJeuQuadrille<Piece> _plateau;
        public MouvementNonObstrue(Deplacement deplacement, PlateauJeuQuadrille<Piece> plateau)
        {
            _deplacementVoulu = deplacement;
            _plateau = plateau;
        }

        public bool EstRespectee()
        {
            if (_deplacementVoulu.PieceChoisiePeutSauter() || EstCheminLibre())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string ObtenirMessageErreur()
        {
            return "La pièce " + _deplacementVoulu.ObtenirTypePieceChoisie() + " ne peut sauter par dessus les autres pièces";
        }

        private bool EstCheminLibre()
        {
            Piece contenuCase;
            IList<CoordonneesPlateau> cases = _plateau.ObtenirCasesSurChemin(_deplacementVoulu.Segment);
            foreach (CoordonneesPlateau laCase in cases)
            {
                contenuCase = _plateau.GetContenuCase(laCase);
                if (contenuCase != null) //au moins une obstruction
                {
                    return false;
                }
            }
            return true;
        }
    }
}

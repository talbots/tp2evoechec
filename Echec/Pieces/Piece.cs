﻿using JeuSociete;

namespace Echec
{
    public abstract class Piece
    {
        public bool ADejaBouger { get; set; }
        public virtual Couleur Couleur { get; set; }
        public abstract TypesPiece Type {get;}

        protected Piece() { }

        protected Piece(Couleur couleur)
        {
            Couleur = couleur;
        }

        public virtual bool PeutSauter()
        {
            return false;
        }

        public abstract bool DeplacementPossible(SegmentPlateau deplacementVoulu);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JeuSociete;

namespace Echec
{
    class Roi : Piece 
    {
      
        public override TypesPiece Type
        {
            get
            {
                return TypesPiece.Roi;
            }
        }

        public override bool DeplacementPossible(SegmentPlateau deplacementVoulu)
        {
            return deplacementVoulu.NombreCases() == 1;
        }

    }
}

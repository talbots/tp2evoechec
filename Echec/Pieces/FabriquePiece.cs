﻿using System;

namespace Echec
{
    class FabriquePiece
    {
        public Piece FabriquerPiece(TypesPiece type, Couleur couleur)
        {
            Piece pieceVoulue = null;
            switch (type)
            {
                case TypesPiece.Pion :
                    pieceVoulue = new Pion( );
                    break;
                case TypesPiece.Fou:
                    pieceVoulue = new Fou();
                    break;
                case TypesPiece.Cavalier:
                    pieceVoulue = new Cavalier();
                    break;
                case TypesPiece.Tour:
                    pieceVoulue = new Tour();
                    break;
                case TypesPiece.Reine:
                    pieceVoulue = new Reine();
                    break;
                case TypesPiece.Roi:
                    pieceVoulue = new Roi();
                    break;
                default:
                    throw new Exception();

            }
            pieceVoulue.Couleur = couleur;
            return pieceVoulue;
        }

    }
}

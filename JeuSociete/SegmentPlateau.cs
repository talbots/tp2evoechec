﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuSociete
{
    public class SegmentPlateau
    {
        public CoordonneesPlateau Origine { get; set; }
        public CoordonneesPlateau Destination { get; set; }
        public SegmentPlateau(CoordonneesPlateau depart, CoordonneesPlateau arrivee)
        {
            Origine = depart;
            Destination = arrivee;
        }

        public bool EstSegmentDroit()
        {
            if (Origine.X != Destination.X && Origine.Y == Destination.Y)
            {
                return true;
            }
            if (Origine.X == Destination.X && Origine.Y != Destination.Y)
            {
                return true;
            }
            return false;
        }

        public bool EstSegmentDiagonal45Degres()
        {
            int offsetX = Origine.X - Destination.X;
            int offsetY = Origine.Y - Destination.Y;
            if ((Math.Abs(offsetX) == Math.Abs(offsetY)))
            {
                return true;
            }
            return false;
        }

        public bool EstSegmentVertical()
        {
            Boolean abcisseChangee = (Origine.X != Destination.X);
            Boolean ordonneeChangee = (Origine.Y != Destination.Y);
            return (!abcisseChangee && ordonneeChangee);
        }

        public bool EstSegmentHorizontal()
        {
            Boolean abcisseChangee = (Origine.X != Destination.X);
            Boolean ordonneeChangee = (Origine.Y != Destination.Y);
            return (abcisseChangee && !ordonneeChangee);
        }

        public int NombreCases()
        {
            if (EstSegmentHorizontal() || EstSegmentDiagonal45Degres())
            {
                return Math.Abs(Origine.X - Destination.X);
            }
            if (EstSegmentVertical())
            {
                return Math.Abs(Origine.Y - Destination.Y);
            }
            return 0;
        }
    }
}

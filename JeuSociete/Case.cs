﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuSociete
{
    public class Case<T>
    {
        T _contenuCase;
        public Case(T contenuCase)
        {
            _contenuCase = contenuCase;
        }

        public Case(){}

        public T ContenuCase { get; set; }

        public bool EstCaseVide()
        {
            if (_contenuCase == null)
            {
                return true;
            }
            return false;
        }

        public void ViderCase()
        {
            _contenuCase = default(T);
        }
    }
}
